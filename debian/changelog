charliecloud (0.38-2) unstable; urgency=medium

  * Backport upstream fix: Do not use deprecated get_html_theme_path
    for newer Sphinx versions (Closes: #1086621)
  * Switch to new upstream repository URL in
    - d/watch
    - d/copyright
    - d/upstream/metadata
  * charliecloud-doc: Add dependency on libjs-jquery and patch out
    bundled jquery.js

 -- Peter Wienemann <wiene@debian.org>  Sat, 23 Nov 2024 17:04:02 +0100

charliecloud (0.38-1) unstable; urgency=medium

  * New upstream version 0.38
  * Drop patch pr1856 (applied upstream)
  * Drop patch pr1859 (applied upstream)
  * Make dependencies of binary packages binNMU safe
  * Update my email address in uploaders field
  * Bump Standards-Version to 4.7.0 (no changes required)

 -- Peter Wienemann <wiene@debian.org>  Sat, 15 Jun 2024 21:23:24 +0200

charliecloud (0.37-2) unstable; urgency=medium

  * Add patch from upstream PR 1859 to fix FTBFS issue on arm{el,hf}
    (Closes: #1065793)

 -- Peter Wienemann <fossdev@posteo.de>  Sat, 16 Mar 2024 11:47:02 +0100

charliecloud (0.37-1) unstable; urgency=medium

  * New upstream version 0.37 (Closes: #1063467)
  * Add new man page for ch-completion.bash
  * Replace obsolete build dependency pkg-config by pkgconf
  * Run reprotest with diffoscope
  * Add patch from upstream PR 1856 to fix reproducibility issue

 -- Peter Wienemann <fossdev@posteo.de>  Thu, 07 Mar 2024 20:06:12 +0100

charliecloud (0.36-1) unstable; urgency=medium

  * New upstream version 0.36
  * Drop patch pr1789 (applied upstream)
  * Add pkg-config to build dependencies
  * Drop buildah and docker.io from dependencies of charliecloud-builders
  * Add bash tab completion for command ch-convert
  * d/t/tutorial:
    - Do not test builders buildah and docker
    - Use --quiet option for ch-convert and ch-run
  * d/t/control: Fix dependencies on Charliecloud packages
    charliecloud-tests -> charliecloud, charliecloud-doc
  * Update upstream and Debian packaging copyright years

 -- Peter Wienemann <fossdev@posteo.de>  Sat, 03 Feb 2024 21:52:40 +0100

charliecloud (0.35-7) unstable; urgency=medium

  * Add patch from upstream PR 1789 to add seccomp support for s390x
  * d/t/tutorial: Build images using seccomp mode on all architectures
  * d/t/control: Re-enable autopkgtest on s390x architecture

 -- Peter Wienemann <fossdev@posteo.de>  Sat, 02 Dec 2023 20:45:00 +0100

charliecloud (0.35-6) unstable; urgency=medium

  * d/tests/control:
    Skip autopkgtest on s390x (Closes: #1055833)

 -- Peter Wienemann <fossdev@posteo.de>  Sun, 12 Nov 2023 21:08:58 +0100

charliecloud (0.35-5) unstable; urgency=medium

  * d/tests/tutorial:
    Use fakeroot root emulation mode for ch-image build on s390x

 -- Peter Wienemann <fossdev@posteo.de>  Sun, 12 Nov 2023 12:23:09 +0100

charliecloud (0.35-4) unstable; urgency=medium

  * d/tests/tutorial:
    Add option --network=host to (disabled) buildah bud
  * d/tests/control:
    Restrict autopkgtest to AlmaLinux 8 architectures

 -- Peter Wienemann <fossdev@posteo.de>  Fri, 10 Nov 2023 20:56:45 +0100

charliecloud (0.35-3) unstable; urgency=medium

  * d/tests/tutorial:
    Use docker build with --network=host option
  * d/rules:
    - Remove obsolete code to patch out jquery.js and underscore.js
    - Patch out bundled sphinx_highlight.js

 -- Peter Wienemann <fossdev@posteo.de>  Sun, 05 Nov 2023 20:40:39 +0100

charliecloud (0.35-2) unstable; urgency=medium

  * d/tests/tutorial:
    - Retry image build in case of failure
    - Do not test builder buildah

 -- Peter Wienemann <fossdev@posteo.de>  Sat, 04 Nov 2023 20:08:52 +0100

charliecloud (0.35-1) unstable; urgency=medium

  * New upstream version 0.35
  * Drop patch adjust-test-suite-path (superseded by upstream change)
  * charliecloud-tests.install: Adjust to upstream's new directory structure
  * Add rsync as recommended dependency for charliecloud-builders
  * Rewrite autopkgtest to test examples from the Charliecloud tutorial

 -- Peter Wienemann <fossdev@posteo.de>  Wed, 01 Nov 2023 20:12:12 +0100

charliecloud (0.34-1) unstable; urgency=medium

  * New upstream version 0.34
  * Update upstream copyright notice

 -- Peter Wienemann <fossdev@posteo.de>  Sun, 24 Sep 2023 17:52:25 +0200

charliecloud (0.33-1) unstable; urgency=medium

  [ Peter Wienemann ]
  * New upstream version 0.33
  * Drop patch pr1550 (fixed upstream)
  * Remove ch-ssh and its man page (removed upstream)
  * Set permission of libsotest-fi.so and approved-trailing-whitespace
    to 0644
  * Add bash tab completion for commands ch-run and ch-image

  [ Peter Kvillegård ]
  * Add Swedish translation of debconf template (closes: #1041671)

 -- Peter Wienemann <fossdev@posteo.de>  Sun, 30 Jul 2023 11:50:18 +0200

charliecloud (0.31-1) unstable; urgency=medium

  * New upstream version 0.31
  * Add patch from upstream PR 1550
  * Add libfuse3-dev and libsquashfuse-dev to build dependencies
  * Bump Standards-Version to 4.6.2 (no changes required)
  * Update copyright years of upstream code
  * Update copyright years for Debian packaging

 -- Peter Wienemann <fossdev@posteo.de>  Sun, 29 Jan 2023 13:36:46 +0100

charliecloud (0.30-1) unstable; urgency=medium

  * New upstream version 0.30

 -- Peter Wienemann <fossdev@posteo.de>  Sun, 20 Nov 2022 19:34:58 +0100

charliecloud (0.29-1) unstable; urgency=medium

  * New upstream version 0.29
  * Update Lintian override due to tag renaming: Use new name
    debian-watch-does-not-check-openpgp-signature

 -- Peter Wienemann <fossdev@posteo.de>  Sat, 03 Sep 2022 20:35:21 +0200

charliecloud (0.28-1) unstable; urgency=medium

  * New upstream version 0.28
  * d/rules: Fix permissions of build cache related Dockerfiles
  * Fix Lintian overrides to get rid of mismatched-override warnings
  * Bump Standards-Version to 4.6.1 (no changes required)
  * Add git as recommended dependency of charliecloud-builders

 -- Peter Wienemann <fossdev@posteo.de>  Sat, 27 Aug 2022 11:18:38 +0200

charliecloud (0.27-1) unstable; urgency=medium

  * New upstream version 0.27
  * Remove deprecated commands and their man pages
  * Remove patch pr1275 (integrated upstream)
  * Add patch to remove misc/m4 as local Autoconf macro directory
  * Add pigz as suggested dependency of charliecloud-builders
  * Update copyright information of upstream code
  * Make charliecloud-doc an absolute dependency of charliecloud-tests
  * Add sudo as suggested dependency of charliecloud-tests
  * Add sudo as dependency of builder-docker unit test

 -- Peter Wienemann <fossdev@posteo.de>  Sun, 15 May 2022 17:52:55 +0200

charliecloud (0.26-1) unstable; urgency=medium

  * New upstream version 0.26
  * Drop patch pr1194 (merged upstream)
  * Update copyright information for file misc/m4/ax_pthread.m4
  * Remove obsolete commands ch-mount and ch-umount and their man pages
  * Add new command ch-convert and its man page to charliecloud-builders
  * Set permissions of README file in test fixtures directory to 0644
  * charliecloud-tests: Add lintian override for
    package-contains-documentation-outside-usr-share-doc
  * charliecloud-builders: Drop obsolete lintian override
    package-contains-no-arch-dependent-files
  * Update copyright years for Debian packaging
  * Add upstream patch to fix FTBFS issue on i386
  * Run 'wrap-and-sort -ask' on packaging files

 -- Peter Wienemann <fossdev@posteo.de>  Sun, 30 Jan 2022 11:06:08 +0100

charliecloud (0.25-1) unstable; urgency=medium

  * New upstream version 0.25
  * d/rules: Make dh_autoreconf use upstream's autogen.sh script
  * Add patch from upstream PR 1194 to fix autogen.sh failure
  * d/rules: Run configure with --disable-bundled-lark option
  * Remove installation of ch-grow and its man page (dropped upstream)
  * d/rules: Fix permissions of various test suite files
  * Add d/clean to make sure all build remnants are cleaned up
  * Bump Standards-Version to 4.6.0 (no changes required)
  * d/README.Debian: User namespaces are enabled by default since Bullseye
  * Update copyright years

 -- Peter Wienemann <fossdev@posteo.de>  Wed, 20 Oct 2021 17:47:52 +0200

charliecloud (0.21-1) unstable; urgency=medium

  * New upstream version 0.21
  * Drop patch move-charliecloud-man-page-to-section7.patch (applied upstream)
  * d/charliecloud-builders.{install,manpages}: Add ch-image builder
  * Bump Standards-Version to 4.5.1 (no changes required)
  * Override Lintian's duplicate-files tag for various example files

 -- Peter Wienemann <fossdev@posteo.de>  Sun, 20 Dec 2020 15:44:37 +0100

charliecloud (0.20-1) unstable; urgency=medium

  * New upstream version 0.20
  * d/rules: Drop charliecloud man page removal
  * Add patch to move charliecloud man page to section 7
  * Add charliecloud man page to charliecloud-common package

 -- Peter Wienemann <fossdev@posteo.de>  Sun, 25 Oct 2020 18:07:39 +0100

charliecloud (0.19-1) unstable; urgency=medium

  * New upstream version 0.19
  * charliecloud-builders: Remove ch-tug (now integrated in ch-grow)
  * Override Lintian tag package-contains-documentation-outside-usr-share-doc
    for files version.txt and files_inferrable.txt

 -- Peter Wienemann <fossdev@posteo.de>  Sat, 26 Sep 2020 15:42:28 +0200

charliecloud (0.18-1) unstable; urgency=medium

  * New upstream version 0.18
  * New suggested package for charliecloud-tests: shellcheck
  * charliecloud-tests: Also install .dockerignore in test directory
  * charliecloud-builders: Add python3-requests to Depends
  * charliecloud-{common,builders}: Mark packages Multi-Arch: foreign

 -- Peter Wienemann <fossdev@posteo.de>  Sat, 22 Aug 2020 20:37:35 +0200

charliecloud (0.16-3) unstable; urgency=medium

  * d/control: Make builder's docker.io dependence arch-dependent
  * Make sure Docker builder is only tested on archs where Docker is available
  * builders: Suppress Lintian tag package-contains-no-arch-dependent-files
  * Remove suppression of Lintian warning due to embedded language_data.js
  * d/rules: Patch out bundled language_data.js

 -- Peter Wienemann <fossdev@posteo.de>  Wed, 05 Aug 2020 17:01:10 +0200

charliecloud (0.16-2) unstable; urgency=medium

  * d/rules: Add a comment why dh_sphinxdoc is not used
  * d/{rules,control}: Patch out bundled doctools.js and searchtools.js,
    update charliecloud-doc dependencies accordingly
  * Suppress Lintian warning because of embedded language_data.js

 -- Peter Wienemann <fossdev@posteo.de>  Sun, 26 Jul 2020 20:48:34 +0200

charliecloud (0.16-1) unstable; urgency=medium

  * New upstream version 0.16 (closes: #958327)
  * d/patches: Remove outdated patch use-python3
  * Switch to upstream's autotools based build system
  * d/control: Update dependencies and suggestions
  * d/control: Bump Standards-Version to 4.5.0 (no changes required)
  * d/s/lintian-overrides: Suppress debian-watch-does-not-check-gpg-signature
  * d/control: Add 'Rules-Requires-Root: no'
  * Add d/upstream/metadata
  * Update d/copyright:
    - Update copyright years
    - Update upstream's copyright clauses
    - Add copyright for misc/m4/*.m4 files
  * Split up package structure into the following packages:
    - charliecloud
    - charliecloud-common
    - charliecloud-runtime
    - charliecloud-builders
    - charliecloud-tests
    - charliecloud-doc
  * Suppress lintian warning shared-library-lacks-prerequisites
  * d/control: Bump debhelper-compat level to 13
  * d/patches: Add adjust-test-suite-path.patch
  * Move debconf related parts from charliecloud to charliecloud-common package
  * d/charliecloud-common.prerm: Remove /usr/lib/charliecloud/__pycache__
  * d/tests: Add tests using Docker based builder

 -- Peter Wienemann <fossdev@posteo.de>  Sun, 05 Jul 2020 18:19:46 +0200

charliecloud (0.9.10-1) unstable; urgency=medium

  * New upstream version 0.9.10
  * Update copyright years
  * Bump debhelper compat level to 12 and switch to debhelper-compat
  * Adjust debhelper config files to upstream's new directory structure
  * Switch to Standards-Version 4.4.0 (no changes required)
  * Add d/salsa-ci.yml file
  * Switch to python3 (closes: #936290)
    - Add patch to switch to python3
    - d/control: Switch to python3 build dependencies

 -- Peter Wienemann <fossdev@posteo.de>  Sun, 08 Sep 2019 20:23:14 +0200

charliecloud (0.9.6-1) unstable; urgency=medium

  * New upstream version 0.9.6

 -- Peter Wienemann <fossdev@posteo.de>  Fri, 14 Dec 2018 20:09:40 +0100

charliecloud (0.9.5-1) unstable; urgency=medium

  * New upstream version 0.9.5

 -- Peter Wienemann <fossdev@posteo.de>  Sat, 08 Dec 2018 22:19:36 +0100

charliecloud (0.9.4-1) unstable; urgency=medium

  * Fix lintian warning missing-license-paragraph-in-dep5-copyright
  * New upstream version 0.9.4

 -- Peter Wienemann <fossdev@posteo.de>  Sun, 18 Nov 2018 15:56:48 +0100

charliecloud (0.9.3-1) unstable; urgency=medium

  * Update debian/copyright file
    - Update years
    - Adjust indentation
    - Remove section for bats (it is not included in upstream tarballs)
    - Add section for Debian packaging
  * New upstream version 0.9.3

 -- Peter Wienemann <fossdev@posteo.de>  Sat, 06 Oct 2018 22:09:45 +0200

charliecloud (0.9.2-1) unstable; urgency=medium

  [ Helge Kreutzmann ]
  * Add German translation of debconf template (closes: #906186)

  [ Peter Wienemann ]
  * Update my email address in list of uploaders
  * New upstream version 0.9.2
  * Switch to Standards-Version 4.2.1 (no changes required)

 -- Peter Wienemann <fossdev@posteo.de>  Fri, 14 Sep 2018 22:19:32 +0200

charliecloud (0.9.1-1) unstable; urgency=medium

  * New upstream version 0.9.1
  * Switch to Standards-Version 4.2.0 (no changes required)

 -- Peter Wienemann <wienemann@physik.uni-bonn.de>  Sat, 11 Aug 2018 22:22:31 +0200

charliecloud (0.9.0-1) unstable; urgency=medium

  [ Lucas Nussbaum ]
  * Add missing changes to changelog entry for 0.2.5-1

  [ Peter Wienemann ]
  * changelog: Correct contributor of Russian translation
  * changelog: Remove duplicate entries
  * New upstream version 0.9.0

  [ Adriano Rafael Gomes ]
  * Add Brazilian Portuguese translation of debconf template (closes: #903354)

 -- Lucas Nussbaum <lucas@debian.org>  Mon, 16 Jul 2018 20:10:38 +0200

charliecloud (0.2.5-1) unstable; urgency=medium

  [ Alban Vidal ]
  * Add French translation of debconf template (closes: #898831)
  * Update debconf template: Use systemctl to restart procps (closes: #898209)
  * Update all po-debconf files with systemctl

  [ Peter Wienemann ]
  * Update po files due to update of debconf template
  * Update README.Debian to get description in sync with debconf template
  * Add debconf-updatepo to "clean" target in debian/rules file
  * charliecloud-doc: Mark package Multi-Arch: foreign
  * New upstream version 0.2.4
  * Adapt debian/rules file to upstream's removal of COPYRIGHT file
  * Remove duplicate doc-src clean-up in debian/rules
  * Add pv to the list of suggested packages for charliecloud
  * Switch to Standards-Version 4.1.4 (no changes required)
  * Update es.po file: Use systemctl to restart procps
  * New upstream version 0.2.5
  * Line rearrangements in ru.po file (performed by debconf-updatepo)

  [ Lev Lamberov ]
  * Add Russian translation of debconf template (closes: #898162)

  [ Rui Branco ]
  * Add Portuguese translation of debconf template (closes: #898498)

  [ Frans Spiesschaert ]
  * Add Dutch translation of debconf template (closes: #899026)

  [ Jonathan Bustillos ]
  * Add Spanish translation of debconf template (closes: #900715)

 -- Lucas Nussbaum <lucas@debian.org>  Thu, 14 Jun 2018 16:27:40 +0200

charliecloud (0.2.3-2) unstable; urgency=medium

  [ Peter Wienemann ]
  * Check unprivileged_userns_clone kernel setting with cat instead of sysctl

  [ Lucas Nussbaum ]
  * Prepare changelog for upload.

 -- Lucas Nussbaum <lucas@debian.org>  Mon, 30 Apr 2018 15:41:33 +0200

charliecloud (0.2.3-1) unstable; urgency=medium

  [ Peter Wienemann ]
  * Restrict architecture to linux-any. Closes: #888395
  * Extend list of uploaders.
  * New upstream version 0.2.3.
  * Add man pages.
  * Display a warning if unprivileged user namespaces are disabled.

  [ Lucas Nussbaum ]
  * Prepare changelog for upload.

 -- Lucas Nussbaum <lucas@debian.org>  Tue, 24 Apr 2018 21:22:33 +0200

charliecloud (0.2.3~git20171120.1a5609e-2) unstable; urgency=medium

  * Switch Maintainer to Debian HPC team

 -- Lucas Nussbaum <lucas@debian.org>  Sun, 07 Jan 2018 16:56:19 +0100

charliecloud (0.2.3~git20171120.1a5609e-1) unstable; urgency=medium

  * Initial release (Closes: #882259)

 -- Lucas Nussbaum <lucas@debian.org>  Sun, 07 Jan 2018 09:35:19 +0100
